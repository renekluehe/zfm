 Rails.application.routes.draw do

  namespace :admin do
    resources :entry_points, only: [:index]
    resources :panels
    resources :event_questions
    resources :visitor_questions
    # resources :ratings
    # resources :rating_schemas
    # resources :rating_sessions
    resources :rating_steps, only: [:create, :edit, :update]

    root to: "entry_points#index"
  end

  root to: 'visitor/panels#index'

  namespace :interviewer do
    get 'main', to: 'main#index', as: 'main_index'

    resources :rating_sessions, only: [:new, :create]

    resources :panels, only: [:index] do
      resources :questions, only: [:new, :create]
    end
  end

  namespace :redakteur do
    resources :panels, only: [:show] do
      put 'update_question', on: :collection
      resources :selected_questions, only: [:create, :update, :destroy]
    end
  end

  namespace :moderator do
    get  'panels-zfm-16-55/:id(.:format)', to: 'panels#show', as: 'panel'
    post 'panels-zfm-16-55/:panel_id/current_questions', to: 'current_questions#create', as: 'panel_current_questions'
    delete 'panels-zfm-16-55/:panel_id/current_questions/:id', to: 'current_questions#destroy', as: 'panel_current_question'

    # resources :panels, only: [:show] do
    #   resources :current_questions, only: [:create, :destroy]
    # end
  end

  namespace :playout do
    resources :panels, only: [:show]
  end

  namespace :operator do
    resources :panels, only: [:index] do
      resources :questions, only: [:new, :create]
    end
  end

  namespace :visitor do
    resources :panels, only: [:index] do
      resources :questions, only: [:new, :create]
    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
