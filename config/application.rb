require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Zfm
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.i18n.default_locale = :de

    config.time_zone = 'Europe/Berlin'

    config.assets.paths << Rails.root.join("app", "assets", "fonts")

    # see https://blog.botreetechnologies.com/rails-customize-and-improve-your-generators-2b5b2f4b5666
    config.generators do |g|
      g.stylesheets false
      g.javascripts false
      g.javascript_engine :js
      g.test_framework :test_unit, views: false
    end

  end
end
