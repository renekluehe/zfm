# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180328115238) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "panels", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "current_question_id"
    t.integer "selected_questions", default: [], array: true
    t.integer "finished_questions", default: [], array: true
  end

  create_table "questions", force: :cascade do |t|
    t.string "type"
    t.string "content"
    t.integer "priority", default: 0
    t.bigint "panel_id"
    t.bigint "rating_schema_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "prefix"
    t.string "author"
    t.index ["panel_id"], name: "index_questions_on_panel_id"
    t.index ["rating_schema_id"], name: "index_questions_on_rating_schema_id"
    t.index ["type"], name: "index_questions_on_type"
  end

  create_table "rating_schemas", force: :cascade do |t|
    t.string "name"
    t.integer "rating_steps", default: [], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["rating_steps"], name: "index_rating_schemas_on_rating_steps", using: :gin
  end

  create_table "rating_sessions", force: :cascade do |t|
    t.text "feedback"
    t.bigint "visitor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["visitor_id"], name: "index_rating_sessions_on_visitor_id"
  end

  create_table "rating_steps", force: :cascade do |t|
    t.string "name"
    t.integer "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ratings", force: :cascade do |t|
    t.bigint "question_id"
    t.bigint "rating_step_id"
    t.bigint "rating_session_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["question_id"], name: "index_ratings_on_question_id"
    t.index ["rating_session_id"], name: "index_ratings_on_rating_session_id"
    t.index ["rating_step_id"], name: "index_ratings_on_rating_step_id"
  end

  create_table "visitors", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "title"
    t.string "organization"
    t.string "town"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "questions", "panels"
  add_foreign_key "questions", "rating_schemas"
  add_foreign_key "rating_sessions", "visitors"
  add_foreign_key "ratings", "questions"
  add_foreign_key "ratings", "rating_sessions"
  add_foreign_key "ratings", "rating_steps"
end
