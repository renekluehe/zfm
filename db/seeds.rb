# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

panels = Panel.create!([
  { title: 'Panel 1 „PARTNERschafft MEHRWERT : Gemeinsam mehr erreichen!"', description: 'Wie können Partnerschaften einen Beitrag dazu leisten, Wirtschaftsbeziehungen zu stabilisieren, die Nachhaltigkeit zu erhöhen und Risikomanagement bei den derzeitigen Verwerfungen in der bestehenden Weltwirtschaftsordnung zu betreiben?' },
  { title: 'Panel 2 "DEAL: This will be really great!: Verlässliche und nachhaltige Partnerschaften in den USA als Erfolgsfaktor in Zeiten des Umbruchs"', description: 'Wie können Vertreter aus Unternehmen und Wissenschaft in Deutschland und den USA durch zuverlässige Partnerschaften den Unsicherheiten standhalten, die durch Kündigungen von Abkommen und Infragestellung von langjährigen Beziehungen hervorgerufen werden?' },
  { title: 'Panel 3 "Ein Mausklick zum Geschäftserfolg: Digitalisierung als Chance für den Eintritt in den chinesischen Markt?“', description: 'Wie können sich kleine oder mittelständische Unternehmen durch E-Commerce die Möglichkeiten auf diesem riesigen Markt erschließen? Welchen Herausforderungen muss man sich stellen, welche Risiken birgt dies, und welche Chancen bieten sich?' },
])

# rating_steps = RatingStep.create!([
#   { name: 'hohes Interesse', value: 100 },
#   { name: 'Interesse', value: 66 },
#   { name: 'Interessiert weniger', value: 33 },
#   { name: 'Interessiert gar nicht', value: 0 }
# ])

# rating_schemas = RatingSchema.create!([
#   { name: 'Standard', rating_steps: rating_steps.map(&:id) },
#   { name: 'Einfach', rating_steps: [rating_steps.first.id, rating_steps.last.id] }
# ])

# Fragebogen
event_question_data = [
  {
    question: "Welche Art eines international strategischen Partners spielt für die Erschließung internationaler Märkte in Zukunft für Ihr Unternehmen eine bedeutende Rolle?",
    rating_steps: [
      { name: 'Vertrieb / Distributionsnetzwerk / Handelsvertreter', value: 100 },
      { name: 'Kooperationspartner Forschung / Wissenschaft', value: 66 },
      { name: 'Partner im Hinblick auf Ihre Produktion, z. B Zulieferer', value: 33 },
      { name: 'Ausbildung / Qualifizierung', value: 0 }
    ]
  },
  {
    question: "Welche internationalen Märkte werden für Ihr Unternehmen zukünftig eine größere Rolle spielen?",
    rating_steps: [
      { name: 'Europa', value: 100 },
      { name: 'Asien', value: 66 },
      { name: 'Amerika', value: 33 },
      { name: 'Afrika', value: 0 }
    ]
  },
  {
    question: "Welche der folgenden Maßnahmen der Außenwirtschaftsförderung in Sachsen-Anhalt möchten Sie in Zukunft nutzen, um Zugang zu internationalen Märkten zu erhalten?",
    rating_steps: [
      { name: 'Delegationsreisen / Unternehmerreisen', value: 100 },
      { name: 'Messebeteiligung / Messeförderung', value: 66 },
      { name: 'Ländertage', value: 33 },
      { name: 'Innovationsassistent', value: 0 }
    ]
  },
  {
    question: "Welche finanzielle Förderung bei der Erschließung internationaler Märkte sind Ihnen am wichtigsten?",
    rating_steps: [
      { name: 'Exportförderung', value: 100 },
      { name: 'Messeförderung', value: 66 },
      { name: 'Bürgschaften', value: 33 },
      { name: 'Innovationsassistent', value: 0 }
    ]
  },
  {
    question: "Was sind für Sie die größten Herausforderungen bei der Internationalisierung Ihres Unternehmens und der Erschließung internationaler Märkte?",
    rating_steps: [
      { name: 'fehlendes Know-how / mangelnde Erfahrung', value: 100 },
      { name: 'Ich kenne keine Unterstützungsangebote, die mir bei diesem Prozess helfen.', value: 66 },
      { name: 'Ich habe Probleme, Geschäftspartner im Ausland zu finden.', value: 33 },
      { name: 'begrenzte / nicht ausreichend qualifizierte Personalressourcen', value: 0 }
    ]
  },
  {
    question: "Welche Partner gewinnen bei der Erschließung internationaler Märkte für Sie an Bedeutung?",
    rating_steps: [
      { name: 'Internationale Netzwerke / Cluster', value: 100 },
      { name: 'Partner aus Forschung und Wissenschaft', value: 66 },
      { name: 'Außenhandelskammern / Consulting', value: 33 },
      { name: 'Direkte Geschäftspartner', value: 0 }
    ],
    prefix: 'P1'
  },
  {
    question: "Welche Hürden sehen Sie für Ihr Unternehmen bei der Erschließung des US-amerikanischen Marktes?",
    rating_steps: [
      { name: 'zunehmende Unsicherheit durch das Agieren von Präsident Trump', value: 100 },
      { name: 'zu erwartende Handelsschranken', value: 50 },
      { name: 'Marktzugang: Zoll, Logistik, Unternehmensgründung in den USA', value: 0 }
    ],
    prefix: 'P2'
  },
  {
    question: "Welche Hürden sehen Sie für Ihr Unternehmen bei der Erschließung des chinesischen Marktes?",
    rating_steps: [
      { name: 'Probleme beim Schutz meines geistigen Eigentums', value: 100 },
      { name: 'Marktzugang: Zoll, Logistik, Unternehmensgründung in China', value: 66 },
      { name: 'kulturelle / sprachliche Barrieren ', value: 33 },
      { name: 'die neuen IT Richtlinien Chinas und damit einhergehend mögliche Sicherheitsrisiken für mein Unternehmen', value: 0 }
    ],
    prefix: 'P3'
  }
]

event_questions = []

event_question_data.each_with_index do |data, i|
  rating_steps = RatingStep.create!(data[:rating_steps])
  rating_schema = RatingSchema.create!(name: "Question-#{i}", rating_steps: rating_steps.map(&:id))

  question = EventQuestion.create!(
    content: data[:question],
    priority: 0,
    rating_schema: rating_schema,
    prefix: data[:prefix]
  )

  event_questions << question
end

# event_questions = (1..10).each_with_object([]) do |i, questions|
#   question = EventQuestion.create!(
#     content: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec.",
#     priority: 11-i,
#     rating_schema: (i < 10) ? rating_schemas.first : rating_schemas.last
#   )
#   question.update(content: "[Nr. #{question.id}] #{question.content}")
#   questions << question
# end

# Bewertungen
event_questions.each do |question|
  question.rating_schema.rating_steps.each do |rs|
    (Random.new.rand(51)+50).times do
      question.ratings.create!(
        rating_step: rs
      )
    end
  end
end

# Zuschauerfragen
visitor_questions_data = [
  "Dies ist eine Zuschauer-Testfrage für den Außenwirtschaftstag 2018.",
  "Und dies ist eine weitere Zuschauer-Testfrage für den Außenwirtschaftstag 2018."
]

panels.each do |panel|
  visitor_questions = visitor_questions_data.each_with_object([]) do |content, questions|
    questions << VisitorQuestion.create!(
      panel: panel,
      content: content,
      author: 'visitor'
    )
  end
end

# Ausgewählte Fragen + Aktuelle Frage
# panels.each do |panel|
#   question_ids  = panel.question_ids.shuffle[0..2]
#   question_ids += event_questions.map(&:id).shuffle[0..1]

#   selected_id = question_ids.shuffle[0]
#   question_ids.delete(selected_id)

#   panel.update(selected_questions: question_ids, current_question_id: selected_id)
# end
