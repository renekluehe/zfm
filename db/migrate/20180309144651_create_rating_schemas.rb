class CreateRatingSchemas < ActiveRecord::Migration[5.1]
  def change
    create_table :rating_schemas do |t|
      t.string :name
      t.integer :rating_steps, array: true, default: []

      t.timestamps
    end

    add_index :rating_schemas, :rating_steps, using: 'gin'

  end
end
