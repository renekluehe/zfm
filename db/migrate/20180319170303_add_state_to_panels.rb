class AddStateToPanels < ActiveRecord::Migration[5.1]
  def change
    add_column :panels, :current_question_id, :bigint
    add_column :panels, :selected_questions, :integer, array: true, default: []
  end
end
