class CreateRatingSteps < ActiveRecord::Migration[5.1]
  def change
    create_table :rating_steps do |t|
      t.string  :name
      t.integer :value

      t.timestamps
    end
  end
end
