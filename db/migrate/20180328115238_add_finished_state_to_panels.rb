class AddFinishedStateToPanels < ActiveRecord::Migration[5.1]
  def change
    add_column :panels, :finished_questions, :integer, array: true, default: []
  end
end
