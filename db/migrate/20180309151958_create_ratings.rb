class CreateRatings < ActiveRecord::Migration[5.1]
  def change
    create_table :ratings do |t|
      t.belongs_to :question, foreign_key: true
      t.belongs_to :rating_step, foreign_key: true
      t.belongs_to :rating_session, foreign_key: true

      t.timestamps
    end
  end
end
