class AddPrefixToQuestions < ActiveRecord::Migration[5.1]
  def change
    add_column :questions, :prefix, :string
  end
end
