class CreateQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :questions do |t|
      t.string :type, index: true

      t.string  :content
      t.integer :priority, default: 0

      t.belongs_to :panel, foreign_key: true
      t.belongs_to :rating_schema, foreign_key: true

      t.timestamps
    end
  end
end
