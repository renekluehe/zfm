class AddAuthorToQuestions < ActiveRecord::Migration[5.1]
  def change
    add_column :questions, :author, :string
  end
end
