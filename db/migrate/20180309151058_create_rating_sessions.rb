class CreateRatingSessions < ActiveRecord::Migration[5.1]
  def change
    create_table :rating_sessions do |t|
      t.text :feedback
      t.belongs_to :visitor, foreign_key: true

      t.timestamps
    end
  end
end
