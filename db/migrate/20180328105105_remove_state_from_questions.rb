class RemoveStateFromQuestions < ActiveRecord::Migration[5.1]
  def up
    remove_column :questions, :state
  end

  def down
    add_column :questions, :state, :integer, default: 0
  end
end
