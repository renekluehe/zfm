class AddStateToQuestions < ActiveRecord::Migration[5.1]
  def change
    # 0: default, 1: back from projector
    add_column :questions, :state, :integer, default: 0
  end
end
