class Moderator::CurrentQuestionsController < ApplicationController

  # moderator/panels/1/current_questions
  def create
    update_current_question(Question.find(params[:question_id]))
  end

  # moderator/panels/1/current_questions/1
  def destroy
    update_current_question(nil)
  end

  private

  def update_current_question(question)

    @panel    = Panel.find(params[:panel_id])
    @question = question

    # return old question to the pool?
    if @panel.current_question
      Rails.logger.debug("---> finished")
      @panel.attributes['finished_questions'] = (@panel.attributes['finished_questions'] << @panel.current_question.id).uniq
    end

    @panel.current_question = @question

    # remove from selection
    if @question
      selected_question_ids = @panel.attributes['selected_questions']
      @panel.attributes['selected_questions'] = selected_question_ids.delete(@question.id)
    end

    @panel.save

    # push back to redakteur
    CurrentQuestionsRelayJob.perform_later(@panel, @question)
    # push forward to playout
    PlayoutRelayJob.perform_later(@panel, @question)

  end

end
