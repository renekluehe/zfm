class Moderator::PanelsController < ApplicationController

  def show
    @panel = Panel.find(params[:id])
  end

end
