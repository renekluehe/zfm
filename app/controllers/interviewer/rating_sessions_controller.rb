class Interviewer::RatingSessionsController < ApplicationController

  def new
    scope = EventQuestion.order('id asc')
    if params[:p] == '0'
      @questions = scope.where(prefix: nil)
    elsif params[:p] == '1'
      @questions = scope.where.not(prefix: nil)
    else
      @questions = scope.all
    end
  end

  def create
    # filter checked rating params
    checked_ratings_params = []

    rating_session_params[:ratings].each do |pos, rating_params|
      if rating_params[:rating_step_id]
        checked_ratings_params << {
          question_id: rating_params[:question_id],
          rating_step_id: rating_params[:rating_step_id]
        }
      end
    end

    if checked_ratings_params.any?

      ActiveRecord::Base.transaction do
        rating_session = RatingSession.create!(
          feedback: rating_session_params[:feedback]
        )
        checked_ratings_params.each do |row|
          rating_session.ratings.create!(
            question_id: row[:question_id],
            rating_step_id: row[:rating_step_id]
          )
        end
      end # transaction

      Panel.all.each do |panel|
        RatingsRelayJob.perform_later(panel)
      end

    end # if

    redirect_to interviewer_main_index_url, notice: "Die Befragung wurde gespeichert"
  end

private
  def rating_session_params
    params.require(:rating_session).permit(:feedback, ratings: [:question_id, :rating_step_id])
  end

end
