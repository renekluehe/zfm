class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :http_authenticate

  private

  def http_authenticate
    if Rails.env == 'staging' || (Rails.env == 'production' && ['redakteur', 'moderator'].include?(current_scope))
      authenticate_or_request_with_http_basic do |username, password|
        username == Rails.application.secrets[:auth_user] && password == Rails.application.secrets[:auth_pass] ||
        username == Rails.application.secrets[:auth_admin_user] && password == Rails.application.secrets[:auth_admin_pass]
      end
    else
      true
    end
  end

  def current_scope
    request.path.split('/')[1]
  end

end

