module Admin
  class RatingStepsController < Admin::ApplicationController
    # To customize the behavior of this controller,
    # you can overwrite any of the RESTful actions. For example:
    #
    # def index
    #   super
    #   @resources = RatingStep.
    #     page(params[:page]).
    #     per(10)
    # end

    # Define a custom finder by overriding the `find_resource` method:
    # def find_resource(param)
    #   RatingStep.find_by!(slug: param)
    # end

    # See https://administrate-prototype.herokuapp.com/customizing_controller_actions
    # for more information

    def edit
      ids = params[:id].split('-')

      event_question = EventQuestion.find(ids[0])
      rating_schema = event_question.rating_schema || RatingSchema.new
      rating_step = rating_schema.rating_steps.find {|rs| rs.value == ids[1].to_i} || RatingStep.new(value: ids[1].to_i)

      render locals: {
        page: Administrate::Page::Form.new(dashboard, rating_step),
        event_question: event_question
      }
    end

    def create
      event_question = EventQuestion.find(params[:event_question_id])

      if event_question.add_rating_step(params[:rating_step])
        redirect_to(
          [:admin, event_question],
          notice: t("administrate.controller.update.create", resource: 'Antwort')
        )
      else
        render :new, locals: {
          page: Administrate::Page::Form.new(dashboard, rating_step),
          event_question: event_question
        }
      end
    end

    def update
      event_question = EventQuestion.find(params[:event_question_id])

      if event_question.update_rating_step(params[:rating_step])
        redirect_to(
          [:admin, event_question],
          notice: t("administrate.controller.update.success", resource: 'Antwort')
        )
      else
        render :edit, locals: {
          page: Administrate::Page::Form.new(dashboard, rating_step),
          event_question: event_question
        }
      end
    end

  end
end
