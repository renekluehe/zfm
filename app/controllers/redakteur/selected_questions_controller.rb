class Redakteur::SelectedQuestionsController < ApplicationController

  # POST redakteur/panels/1/selected_questions
  def create
    @panel    = Panel.find(params[:panel_id])
    @question = Question.find(params[:question_id])

    selected_question_ids = (@panel.attributes['selected_questions'] << @question.id).uniq
    @panel.update(selected_questions: selected_question_ids)

    if @panel.finished_questions.include?(@question)
      finished_question_ids = @panel.attributes['finished_questions'] - [@question.id]
      @panel.update(finished_questions: finished_question_ids)
    end

    SelectedQuestionsRelayJob.perform_later(@panel, @question)

    @question_counts = get_question_counts
  end

  # DELETE redakteur/panels/1/selected_questions/1
  def destroy
    @panel    = Panel.find(params[:panel_id])
    @question = Question.find(params[:id])

    selected_question_ids = @panel.attributes['selected_questions']
    selected_question_ids.delete(@question.id)
    @panel.update(selected_questions: selected_question_ids)

    SelectedQuestionsRelayJob.perform_later(@panel, @question)

    @question_counts = get_question_counts
  end

  # PUT redakteur/panels/1/selected_questions
  def update
    @panel = Panel.find(params[:panel_id])
    ids    = params[:ids].split(',') rescue @panel.attributes['selected_questions']

    selected_question_ids = []
    Question.find(ids).each do |question|
      if (question.is_a?(VisitorQuestion) && @panel.questions.include?(question)) || question.is_a?(EventQuestion)
        selected_question_ids << question.id
      end
    end
    @panel.update(selected_questions: selected_question_ids)

    SelectedQuestionsRelayJob.perform_later(@panel, nil)
  end

private

  def get_question_counts
    {
      'event' => @panel.available_event_questions.count,
      'visitor' => @panel.available_visitor_questions.count,
      'finished' => @panel.finished_questions.count
    }
  end

end
