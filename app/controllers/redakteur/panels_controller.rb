class Redakteur::PanelsController < ApplicationController

  def show
    @panel = Panel.find(params[:id])
  end

  def update_question
    @question = Question.find(params[:pk])
    if @question.update(content: params[:value])
      render plain: 'ok'
    else
      render plain: 'fail'
    end
  end

end
