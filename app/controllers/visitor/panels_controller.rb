class Visitor::PanelsController < ApplicationController

  def index
    @panels = Panel.order('id asc')
  end

end
