class Visitor::QuestionsController < ApplicationController

  def new
    @panel = Panel.find(params[:panel_id])
    @question = VisitorQuestion.new
  end

  def create
    @panel = Panel.find(question_params[:panel_id])
    @question = @panel.questions.create!(
      type: 'VisitorQuestion',
      content: question_params[:content],
      author: 'visitor'
    )

    VisitorQuestionsRelayJob.perform_later(@panel, @question)
    GlobalVisitorQuestionsRelayJob.perform_later(@panel, @question)

    redirect_to visitor_panels_url, notice: "Die Frage wurde gespeichert."
  end

private
  def question_params
    params.permit(:panel_id, :content)
  end

end
