require "administrate/base_dashboard"

class PanelDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    # questions: Field::HasMany,
    questions: Field::HasMany.with_options(class_name: "VisitorQuestion"),
    # current_question: Field::BelongsTo.with_options(class_name: "Question"),
    id: Field::Number,
    title: Field::Text,
    description: Field::Text,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    # current_question_id: Field::Number,
    # selected_questions: Field::Number,
    # finished_questions: Field::Number,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :title,
    :questions,
    # :current_question,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    # :current_question,
    :id,
    :title,
    :description,
    :questions,
    :created_at,
    :updated_at,
    # :current_question_id,
    # :selected_questions,
    # :finished_questions,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    #:questions,
    # :current_question,
    :title,
    :description,
    # :current_question_id,
    # :selected_questions,
    # :finished_questions,
  ].freeze

  # Overwrite this method to customize how panels are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(panel)
  #   "Panel ##{panel.id}"
  # end
end
