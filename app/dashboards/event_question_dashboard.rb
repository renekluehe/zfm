require "administrate/base_dashboard"

class EventQuestionDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    # panel: Field::BelongsTo,
    # rating_schema: Field::BelongsTo,
    ratings: Field::HasMany,
    id: Field::Number,
    # type: Field::String,
    content: Field::Text,
    # priority: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    # author: Field::String,
    rating_steps: RatingStepsField
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :content,
    # :panel,
    # :rating_schema,
    :ratings,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :content,
    :rating_steps,
    :created_at,
    :updated_at,
    # :panel,
    # :rating_schema,
    # :type,
    # :priority,
    # :author,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    # :panel,
    # :rating_schema,
    # :ratings,
    # :type,
    :content,
    # :priority,
    # :author,
  ].freeze

  # Overwrite this method to customize how event questions are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(event_question)
    "#{EventQuestion.model_name.human} ##{event_question.id}"
  end
end
