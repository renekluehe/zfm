require "administrate/base_dashboard"

class RatingSessionDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    ratings: Field::HasMany,
    id: Field::Number,
    feedback: Field::Text,
    visitor_id: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :ratings,
    :id,
    :feedback,
    :visitor_id,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :ratings,
    :id,
    :feedback,
    :visitor_id,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :ratings,
    :feedback,
    :visitor_id,
  ].freeze

  # Overwrite this method to customize how rating sessions are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(rating_session)
  #   "RatingSession ##{rating_session.id}"
  # end
end
