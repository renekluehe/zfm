// Push Moderator => Redakteur

App.current_questions_channel = App.cable.subscriptions.create({ channel: 'CurrentQuestionsChannel' }, {
  connected: function() {
    // Called when the subscription is ready for use on the server
  },

  disconnected: function() {
    // Called when the subscription has been terminated by the server
  },

  received: function(data) {
    // Called when there's incoming data on the websocket for this channel

    $('body[data-panel-id="' + data.panel_id + '"] #redakteur-current-questions').html(data.current_question);
    $('body[data-panel-id="' + data.panel_id + '"] #redakteur-selected-questions').html(data.selected_questions);
    $('body[data-panel-id="' + data.panel_id + '"] #redakteur-finished-questions').html(data.finished_questions);
    var count = $('#redakteur-finished-questions .question-list-item').length;
    $('body[data-panel-id="' + data.panel_id + '"] #redakteur-finished-questions-count').text(count);
  }
});
