// Interviewer, Operator, Visitor => Redakteur

App.visitor_questions_channel = App.cable.subscriptions.create({ channel: 'VisitorQuestionsChannel' }, {
  connected: function() {
    // Called when the subscription is ready for use on the server
  },

  disconnected: function() {
    // Called when the subscription has been terminated by the server
  },

  received: function(data) {
    // Called when there's incoming data on the websocket for this channel

    // redakteur
    $('body[data-panel-id="' + data.panel_id + '"] #redakteur-visitor-questions').prepend(data.question);
    $('body[data-panel-id="' + data.panel_id + '"] #redakteur-visitor-questions-count').text($('#redakteur-visitor-questions .question-list-item').length);
  }
});
