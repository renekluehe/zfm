// Push Interviewer => Interviewer, Redakteur

App.ratings_channel = App.cable.subscriptions.create("RatingsChannel", {
  connected: function() {
    // Called when the subscription is ready for use on the server
  },

  disconnected: function() {
    // Called when the subscription has been terminated by the server
  },

  received: function(data) {
    // Called when there's incoming data on the websocket for this channel
    $('#rating-count').html(data.rating_count);
    $('body[data-panel-id="' + data.panel_id + '"] #redakteur-event-questions').html(data.event_questions);
  }
});
