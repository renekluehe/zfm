// Push Interviewer => Interviewers Main

App.global_visitor_questions_channel = App.cable.subscriptions.create({ channel: 'GlobalVisitorQuestionsChannel' }, {
  connected: function() {
    // Called when the subscription is ready for use on the server
  },

  disconnected: function() {
    // Called when the subscription has been terminated by the server
  },

  received: function(data) {
    // Called when there's incoming data on the websocket for this channel

    // interviewer main
    $('#panel-question-count-' + data.visitor_question_count.panel_id).text(data.visitor_question_count.count);
  }
});
