// Push Redakteur => Moderator

App.selected_questions_channel = App.cable.subscriptions.create({ channel: 'SelectedQuestionsChannel' }, {
  connected: function() {
    // Called when the subscription is ready for use on the server
  },

  disconnected: function() {
    // Called when the subscription has been terminated by the server
  },

  received: function(data) {
    // Called when there's incoming data on the websocket for this channel

    // replace content of associated panel
    $('body[data-panel-id="' + data.panel_id + '"] #moderator-selected-questions').html(data.selected_list);
  }
});
