// Push Moderator => Playout

App.playout_channel = App.cable.subscriptions.create({ channel: 'PlayoutChannel' }, {
  connected: function() {
    // Called when the subscription is ready for use on the server
  },

  disconnected: function() {
    // Called when the subscription has been terminated by the server
  },

  received: function(data) {
    // Called when there's incoming data on the websocket for this channel
    $('body[data-panel-id="' + data.panel_id + '"] #playout-current-question').html(data.current_question);
    $('body[data-panel-id="' + data.panel_id + '"] #playout-rating-stats').html(data.rating_stats);
  }
});
