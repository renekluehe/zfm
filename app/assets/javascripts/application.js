// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require rails-ujs
//= require cable
//= require bootstrap
//= require owl.carousel
//= require jquery.slimscroll
//= require Sortable
//= require bootstrap-editable

var reloadWithTurbolinks = (function () {
  var scrollPosition;

  function reload () {
    scrollPosition = [window.scrollX, window.scrollY];
    Turbolinks.visit(window.location.toString(), { action: 'replace' });
  }

  document.addEventListener('turbolinks:load', function () {
    if (scrollPosition) {
      window.scrollTo.apply(window, scrollPosition);
      scrollPosition = null;
    }
  });

  return reload;
})();

;(function($, window) {

  var uiHelperSlimscroll = function(){
    $('[data-toggle="slimscroll"]').each(function() {
      var $this = $(this);

      $this.slimScroll({
        height: $this.data('height') || '200px',
        size: $this.data('size') || '5px',
        position: $this.data('position') || 'right',
        color: $this.data('color') || '#000',
        alwaysVisible: $this.data('always-visible') ? true : false,
        railVisible: $this.data('rail-visible') ? true : false,
        railColor: $this.data('rail-color') || '#999',
        railOpacity: $this.data('rail-opacity') || .3
      });
    });
  };

  // Init slimScroll functionality
  // ----
  // uiHelperSlimscroll();

  // Initialize Tabs
  // ----
  $(document).on('click', '[data-toggle="tabs"] a, .js-tabs a', function(e) {
    e.preventDefault();
    // Turbolinks.visit($(this).attr('href'));
    $(this).tab('show');
  });

  // // show selected tab on reload
  // $(document).on('turbolinks:load', function() {
  //   if (window.location.hash && $('.nav-tabs').length > 0) {
  //     console.log(window.location.hash);
  //     $('.nav-tabs a[href="' + window.location.hash + '"]').tab('show');
  //   }
  // });

  // Sortable list
  // ----
  $.fn.zfmSortableList = function() {

    return this.each(function() {
      Sortable.create(this, {
        ghostClass: "sortable-ghost",
        onUpdate: function(e) {
          var ids = [],
              $form  = $(this.el).closest('form');

          $(e.from).children().each(function() {
            ids.push($(this).data('question-id'));
          });

          $.ajax({
            url: $form.attr('action'),
            type: "PUT",
            data: 'ids=' + ids.join(',')
          });
        }
      });
    });
  };

  $(document).on('turbolinks:load', function() {
    $('#redakteur-selected-questions').zfmSortableList();
  });

  // Flashes
  // ----
  $(document).on('turbolinks:load', function() {
    var flashNotice = $('.flash-notice').text();

    if (flashNotice.length > 0) {
      setTimeout(function() {
        alert(flashNotice);
      }, 100);
    }
  });

  // Inplace Editor
  // ----
  $.fn.editable.defaults.mode = 'inline';
  $.fn.editable.defaults.ajaxOptions = { type: 'PUT' };
  $.fn.editable.defaults.params = function (params) {
    params.authenticity_token = $("meta[name=csrf-token]").attr("content");
    return params;
  };

  $(document).ready(function() {
    $('#redakteur-event-questions .question-content > p').editable();
    $('#redakteur-visitor-questions .question-content > p').editable();
    $('#redakteur-finished-questions .question-content > p').editable();
  });

})(jQuery, window);
