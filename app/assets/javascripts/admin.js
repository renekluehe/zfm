//= require jquery
//= require bootstrap
//= require bootstrap-editable

$.fn.editable.defaults.mode = 'inline';
$.fn.editable.defaults.ajaxOptions = { type: 'PUT' };
$.fn.editable.defaults.params = function (params) {
  params.authenticity_token = $("meta[name=csrf-token]").attr("content");
  return params;
};

$(document).ready(function() {
  $('#edit-questions .question').editable();
});

