class Question < ApplicationRecord
  belongs_to :panel, optional: true
  belongs_to :rating_schema, optional: true

  has_many :ratings, dependent: :destroy

  after_destroy do |question|
    Panel.all.each { |panel| panel.cleanup_question_references! }
  end

  def highest_rating_step
    if rating_schema
      rating_schema.rating_steps.sort_by(&:value).last
    end
  end

  def highest_rating_count
    if rating_schema
      ratings.where(rating_step: highest_rating_step).count
    end
  end

  def overall_rating_count
    if rating_schema
      ratings.count
    end
  end

  def rating_stats
    stats = self.class.empty_rating_stats

    if rating_schema
      overall_count = ratings.count
      step_counts = rating_schema.rating_steps.map { |rating_step| ratings.where(rating_step_id: rating_step.id).count }
      max_step_count = step_counts.max

      if max_step_count > 0
        stats[:overall_rating_count] = overall_count
        stats[:max_step_count] = max_step_count

        rating_schema.rating_steps.each_with_index do |rating_step, i|
          step_count = step_counts[i]
          stats[:rating_step_counts] << { name: rating_step.name, count: step_count, max_count: max_step_count, percent: ((step_count.to_f / max_step_count.to_f) * 100.0).round }
        end
      end
    end

    stats
  end

  def self.empty_rating_stats
    { rating_step_counts: [] }
  end

  # scopes
  # ----

  def self.recent
    order('created_at desc')
  end

end
