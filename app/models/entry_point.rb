class EntryPoint
  include ActiveModel::Model

  attr_accessor :name

  def self.reflections
    {}
  end

  def self.attribute_names
    []
  end
end
