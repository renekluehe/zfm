class EventQuestion < Question
  after_create :ensure_rating_schema!

  def add_rating_step(attributes)
    ensure_rating_schema!

    rating_step = RatingStep.new(name: attributes[:name], value: attributes[:value])

    valid = rating_step.save

    step_ids = rating_schema.attributes['rating_steps']
    step_ids[rating_step.value] = rating_step.id

    valid &&= rating_schema.update(rating_steps: step_ids)

    valid
  end

  def update_rating_step(attributes)
    ensure_rating_schema!

    rating_step = rating_schema.rating_steps.find {|rs| rs.value == attributes[:value].to_i}

    rating_step.update(name: attributes[:name])
  end

  private
    def ensure_rating_schema!
      unless rating_schema
        RatingSchema.create!(name: "Question #{id}", questions: [self])
      end
    end
end
