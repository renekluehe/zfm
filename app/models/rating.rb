class Rating < ApplicationRecord
  belongs_to :question
  belongs_to :rating_step

  belongs_to :visitor, optional: true

end
