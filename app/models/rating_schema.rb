class RatingSchema < ApplicationRecord
  has_many :questions

  MAX_STEPS = 4

  def rating_steps
    RatingStep.order(:value).find(super)
  end

  # def self._update_all_schema_values!
  #   all.each do |rating_schema|
  #     rating_schema.rating_steps.each_with_index do |rating_step,i|
  #       puts "#{i} : #{rating_step.value} : #{rating_step.name}"
  #       rating_step.update!(value: i+1)
  #     end
  #   end
  # end
end
