class Panel < ApplicationRecord
  has_many :questions, dependent: :destroy

  belongs_to :current_question, class_name: 'Question', optional: true

  def available_event_questions
    questions = EventQuestion.where.not(id: except_ids).to_a
    questions.sort_by(&:overall_rating_count).reverse
  end

  def available_visitor_questions
    questions.where.not(id: except_ids).recent
  end

  def finished_questions
    Question.find(super)
  end

  # selected wo/ current
  def selected_questions
    Question.find(super)
  end

  # for availabel lists:
  # ignore selected + current question
  def except_ids
    (attributes['selected_questions'] + attributes['finished_questions'] + [current_question_id]).compact
  end

  def selected_count
    selected_questions.count
  end

  def max_selection_count
    6
  end

  def finished?(question)
    attributes['finished_questions'].include?(question.id)
  end

  def current_question_stats
    current_question.try(:rating_stats) || Question.empty_rating_stats
  end

  def remove_question_references!(question_id)
    if current_question_id == question_id
      self.current_question = nil
    end
    if attributes['selected_questions'].include?(question_id)
      self.selected_questions = attributes['selected_questions'] - [question_id]
    end
    if attributes['finished_questions'].include?(question_id)
      self.finished_questions = attributes['finished_questions'] - [question_id]
    end
    update_columns(
      current_question_id: attributes['current_question_id'],
      selected_questions: attributes['selected_questions'],
      finished_questions: attributes['finished_questions']
    )
  end

  def cleanup_question_references!
    question_ids = ([current_question_id] + attributes['selected_questions'] + attributes['finished_questions']).uniq.compact
    existing_ids = Question.where(id: question_ids).pluck(:id)

    # non-existing ids
    question_ids = question_ids - existing_ids

    question_ids.each do |qid|
      remove_question_references!(qid)
    end
  end

  # --- debug
  def self.debug_reset_all!
    all.each do |panel|
      panel.update!(
        current_question: nil,
        selected_questions: [],
        finished_questions: []
      )
    end
  end
end
