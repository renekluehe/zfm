class Visitor < ApplicationRecord
  has_many :ratings
  has_many :questions, through: :ratings

end
