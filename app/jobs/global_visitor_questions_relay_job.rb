class GlobalVisitorQuestionsRelayJob < ApplicationJob
  queue_as :default

  def perform(panel, question)
    if panel
      ActionCable.server.broadcast "global_visitor_questions",
        panel_id: panel.id,
        visitor_question_count: { panel_id: panel.id, count: panel.questions.count }
    end
  end
end
