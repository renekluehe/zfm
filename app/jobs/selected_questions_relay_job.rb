class SelectedQuestionsRelayJob < ApplicationJob
  queue_as :default

  def perform(panel, question)
    ActionCable.server.broadcast "selected_questions",
      panel_id: panel.id,
      selected_list: Moderator::PanelsController.render(partial: 'moderator/panels/selected_list', locals: { panel: panel })
  end
end
