class VisitorQuestionsRelayJob < ApplicationJob
  queue_as :default

  def perform(panel, question)
    ActionCable.server.broadcast "visitor_questions",
      panel_id: panel.id,
      question: Redakteur::PanelsController.render(partial: 'redakteur/panels/question', locals: { panel: panel, question: question, mode: 'add-to-selection' })
  end
end
