class CurrentQuestionsRelayJob < ApplicationJob
  queue_as :default

  def perform(panel, question)
    data = {}

    data[:panel_id] = panel.id

    if question
      data[:current_question] = Redakteur::PanelsController.render(partial: 'redakteur/panels/question', locals: { panel: panel, question: panel.current_question, mode: nil })
    else
      data[:current_question] = Redakteur::PanelsController.render(partial: 'redakteur/panels/empty_current_question')
    end

    data[:selected_questions] = Redakteur::PanelsController.render(partial: 'redakteur/panels/question', collection: panel.selected_questions, locals: { panel: panel, mode: 'remove-from-selection' })

    data[:finished_questions] = Redakteur::PanelsController.render(partial: 'redakteur/panels/question', collection: panel.finished_questions, locals: { panel: panel, mode: 'add-to-selection' })

    ActionCable.server.broadcast "current_questions", data
  end
end
