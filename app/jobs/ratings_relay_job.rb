class RatingsRelayJob < ApplicationJob
  queue_as :default

  def perform(panel)
    ActionCable.server.broadcast "ratings",
      panel_id: panel.id,
      rating_count: Rating.count,
      event_questions: Redakteur::PanelsController.render(partial: 'redakteur/panels/question', collection: panel.available_event_questions, locals: { panel: panel, mode: 'add-to-selection' })
  end
end

