class PlayoutRelayJob < ApplicationJob
  queue_as :default

  def perform(panel, question)
    ActionCable.server.broadcast "playout",
      panel_id: panel.id,
      current_question: Moderator::PanelsController.render(partial: 'playout/panels/current_question', locals: { panel: panel }),
      rating_stats: Moderator::PanelsController.render(partial: 'moderator/panels/rating_stats', locals: { panel: panel })
  end
end
