require "administrate/field/base"

class RatingStepsField < Administrate::Field::Base
  def to_s
    data
  end

  def rating_steps
    if resource.is_a?(EventQuestion) && resource.rating_schema
      (1..RatingSchema::MAX_STEPS).map do |pos|
        rating_step = resource.rating_schema.rating_steps.find { |rs| rs.value == pos }
        { pos: pos, name: rating_step.try(:name) }
      end
    else
      []
    end
  end

end
