module ApplicationHelper

  def title
    title = ['ZFM']
    if @page_title
      title << @page_title
    end
    title.join(' - ')
  end

  def page_title(page_title)
    @page_title = page_title
  end

  def flashes
    html = "";
    if flash[:notice]
      html += %(<div class="flash flash-notice">#{flash[:notice]}</div>)
    end
    html.html_safe
  end

  # 0: default
  # 1: finished
  def question_state(panel, question)
    panel.finished?(question) ? '1' : '0'
  end

  def question_number(question)
    if question.author == 'operator'
      "V-#{question.id}"
    else
      question.id
    end
  end
end
