class VisitorQuestionsChannel < ApplicationCable::Channel
  def subscribed
    stream_from "visitor_questions"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
    stop_all_streams
  end
end
