class GlobalVisitorQuestionsChannel < ApplicationCable::Channel
  def subscribed
    stream_from "global_visitor_questions"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
    stop_all_streams
  end
end
