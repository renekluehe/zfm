class SelectedQuestionsChannel < ApplicationCable::Channel
  def subscribed
    stream_from "selected_questions"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
    stop_all_streams
  end
end
