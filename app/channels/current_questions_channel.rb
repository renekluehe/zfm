class CurrentQuestionsChannel < ApplicationCable::Channel
  def subscribed
    stream_from "current_questions"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
    stop_all_streams
  end
end
